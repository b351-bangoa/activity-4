<?php require_once './code.php' ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Activity 4</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <!-- Instances -->
        <?php
        $person = new Person('John Smith', '30', 'Quezon City, Metro Manila');
        $student = new Student('John Doe', '20', 'Makati City, Metro Manila');
        $student->setStudentId('2023-1980');
        $employee = new Employee('Mark Blain', '35', 'Pasig City, Metro Manila');
        $employee->setTeam('Team Tech');
        $employee->setRole('Tech Lead');
        ?>

        <h1>Display Information</h1>

        <h3>Personal Information</h3>
        <p><?= 'Name: '.$person->getName()?>
        <p><?= 'Age: '.$person->getAge()?>
        <p><?= 'Address: '.$person->getAddress()?>

        <h3>Student Information</h3>
        <p><?= 'Name: '.$student->getName()?>
        <p><?= 'Age: '.$student->getAge()?>
        <p><?= 'Student ID: '.$student->getStudentId()?>
        <p><?= 'Address: '.$student->getAddress()?>

        <h3>Employee Information</h3>
        <p><?= 'Name: '.$employee->getName()?>
        <p><?= 'Age: '.$employee->getAge()?>
        <p><?= 'Team: '.$employee->getTeam()?>
        <p><?= 'Role: '.$employee->getRole()?>
        <p><?= 'Address: '.$employee->getAddress()?>
    </body>
</html>