<?php
class Person {
    private $name;
    private $age;
    private $address;

    public function __construct($name, $age, $address) {
        $this->name = $name;
        $this->age = $age;
        $this->address = $address;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        return $this->name = $name;
    }

    public function getAge() {
        return $this->age;
    }

    public function setAge($age) {
        return $this->age = $age;
    }

    public function getAddress() {
        return $this->address;
    }

    public function setAddress($address) {
        return $this->address = $address;
    }
}

class Student extends Person {
    protected $studentId;

    public function getStudentId() {
        return $this->studentId;
    }

    public function setStudentId($studentId) {
        return $this->studentId = $studentId;
    }
}

class Employee extends Person {
    protected $team;
    protected $role;

    public function getTeam() {
        return $this->team;
    }

    public function setTeam($team) {
        return $this->team = $team;
    }

    public function getRole() {
        return $this->role;
    }

    public function setRole($role) {
        return $this->role = $role;
    }
}

?>